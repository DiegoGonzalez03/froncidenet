import { Component } from '@angular/core';

@Component({
  selector: 'app-branding',
  template: `
    <a class="matero-branding" href="/">
      
      <span class="matero-branding-name">Registro y Control</span>
    </a>
  `,
})
export class BrandingComponent {}
