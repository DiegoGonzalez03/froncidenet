import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

interface ApiParams {
  urlParams?: HttpParams;
  headerParams?: {[key: string]: string};
}

@Injectable({
  providedIn: 'root'
})
export class ApiRequestService {

  constructor(
    private http: HttpClient,
  ) { }

  getHeaders(optionalHeaders?: any): HttpHeaders {
    let headers;
    if (optionalHeaders) {
      headers = new HttpHeaders();
      for (const prop in optionalHeaders) {
        headers = headers.append(prop, String(optionalHeaders[prop]));
      }
    } else {
      headers = new HttpHeaders();
    }

    headers = headers.append('Content-Type', 'application/json');
    return headers;
  }



  get(url: string, config?: ApiParams): Observable<any> {
    return this.http.get(url,
      { headers: this.getHeaders(config?.headerParams), params: config?.urlParams })
  }

  post(url: string, body: Object, config?: ApiParams): Observable<any> {
    return this.http.post(url, JSON.stringify(body),
      { headers: this.getHeaders(config?.headerParams), params: config?.urlParams })
  }

  put(url: string, body: Object, config?: ApiParams): Observable<any> {
    return this.http.put(url, JSON.stringify(body),
      { headers: this.getHeaders(config?.headerParams), params: config?.urlParams })
  }

  delete(url: string, config?: ApiParams): Observable<any> {
    return this.http.delete(url,
      { headers: this.getHeaders(config?.headerParams), params: config?.urlParams })
  }
}
