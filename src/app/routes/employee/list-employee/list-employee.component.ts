import { Component, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { HttpClient, HttpParams } from '@angular/common/http';
import { UsuarioService } from '../shared/services/usuario.service';
import { Router } from '@angular/router';
import { Employee } from '../shared/models/employee';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
})
export class ListEmployeeComponent {

  currentPage = 0;
  pages = 0;
  users: any[];
  loading: boolean = true;
  dataSource = new MatTableDataSource<any>();

  title = 'pagination';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  reactiveForm: FormGroup;

  constructor(private _usuarioServicio: UsuarioService, private _router: Router,
    private _toastrService: ToastrService,  private _fb: FormBuilder) {

  }

  ngOnInit() {
    this.getData(this.currentPage);
  }

  get size() {
    return this.pages * 10;
  }

  getData(page: number) {
    this._usuarioServicio.paginar(page)
      .subscribe((response: any) => {

        this.loading = false;
        this.users = response.usuarios;
        this.pages = response.numeroPaginas;

        this.dataSource = new MatTableDataSource<any>(this.users);
        this.dataSource._updateChangeSubscription();

      })
  }

  pageChanged(event) {
    this.loading = true;

    this.currentPage = event.pageIndex;

    this.getData(this.currentPage);
  }

  redirectToRegister() {
    this._router.navigate(['/employee/register']);
  }

  deleteEmployee(employee: Employee) {
    const { id } = employee;
    this._usuarioServicio.delete(id).subscribe(() => {
      this._toastrService.success('Se elimino el empleado');
      this.getData(this.currentPage);
    }, () => this._toastrService.error('Error al eliminar empleado'));
  }

  editEmployee(employee: Employee) {
    this._router.navigate(['/employee/update'], { state: { employee } });
  }

  private builderForm() {
    this.reactiveForm = this._fb.group([

    ]);
  }

}
