import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';

const routes: Routes = [
  {
    path: '', component: ListEmployeeComponent,
    data: { titleI18n: 'employee' }
  },
  {
    path: 'register', component: RegisterEmployeeComponent,
    data: { titleI18n: 'employee' }
  },
  {
    path: 'update', component: UpdateEmployeeComponent,
    data: { titleI18n: 'employee' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
