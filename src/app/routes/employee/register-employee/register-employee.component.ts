import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormEmployeeComponent } from '../shared/component/form-employee/form-employee.component';
import { Employee } from '../shared/models/employee';
import { UsuarioService } from '../shared/services/usuario.service';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
})
export class RegisterEmployeeComponent implements OnInit {

  @ViewChild(FormEmployeeComponent) form: FormEmployeeComponent;

  constructor(
    private _employeeService: UsuarioService,
    private _router: Router,
    private _toastr: ToastrService) { }



  ngOnInit(): void {

  }

  onClickCancel() {
    this._router.navigate(['/employee']);
  }

  onClickSave(employee: Employee) {
    console.log(employee);
    this._employeeService.register(employee).subscribe(() => {
      this._toastr.success('Registro exitoso');
      this._router.navigate(['/employee']);
    }, err => this.errorExcepcion(err));
  }

  private errorExcepcion(error: any) {
    const mensaje = error?.error?.mensaje || 'Error desconocido, contacte un administrador';
    this._toastr.error(mensaje);
  }
}
