import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationStart, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { filter } from 'rxjs/operators';
import { FormEmployeeComponent } from '../shared/component/form-employee/form-employee.component';
import { Employee } from '../shared/models/employee';
import { UsuarioService } from '../shared/services/usuario.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
})
export class UpdateEmployeeComponent implements OnInit, AfterViewInit {

  employee: Employee;

  @ViewChild(FormEmployeeComponent) form: FormEmployeeComponent;

  constructor(
    private _dialog: MatDialog,
    private _dateAdapter: DateAdapter<any>,
    private _employeeService: UsuarioService,
    private _router: Router,
    private _toastr: ToastrService) {
      this._router.events
      .pipe(filter((event) => (event instanceof NavigationStart)))
      .subscribe((event: NavigationStart) => this.historialParametros(event));

    const { state } = this._router.getCurrentNavigation().extras;

    if (!state) {
      this._router.navigate(['/employee']);
    }
    this.employee = state.employee;
    }


  ngOnInit(): void {

  }

  ngAfterViewInit() {
    setTimeout(() => this.form.loadData(this.employee));
  }

  onClickCancel() {
    this._router.navigate(['/employee']);
  }

  onClickSave(employee: Employee) {
    const { id } = employee;
    this._employeeService.update(id, employee).subscribe(() => {
      this._toastr.success('Actualizacion exitosa');
    }, err => this.errorExcepcion(err));
  }



  private errorExcepcion(error: any) {
    const mensaje = error?.mensaje || 'Error desconocido, contacte un administrador';
    this._toastr.error(mensaje);
  }

  private historialParametros(event: NavigationStart) {
    if (event.restoredState) {
      this._router.getCurrentNavigation().extras.state = event.restoredState;
      console.warn('restoring navigation id:', event.restoredState.navigationId);
    }
  }
}
