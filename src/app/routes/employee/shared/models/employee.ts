export class Employee {
  id: string;
  primerApellido: string;
  segundoApellido: string;
  primerNombre: string;
  otrosNombres: string;
  pais: string;
  tipoIdentificacion: string;
  numeroIdentificacion: string;
  correo: string;
  fechaIngreso: string;
  area: string;
  estado: string;
  fechaRegistro: string;
}
