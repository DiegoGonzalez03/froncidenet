import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NavigationStart, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Area } from '../../models/area';
import { Country } from '../../models/country';
import { DocumentType } from '../../models/document-type';
import { Employee } from '../../models/employee';
import { AreaService } from '../../services/area.service';
import { CountryService } from '../../services/country.service';
import { DocumentTypeService } from '../../services/document-type.service';

const MAXIMO_CARACTERES_PRIMER_APELLIDO = 20;
const MAXIMO_CARACTERES_SEGUNDO_APELLIDO = 20;
const MAXIMO_CARACTERES_PRIMER_NOMBRE = 20;
const MAXIMO_CARACTERES_OTROS_NOMBRE = 50;
const MAXIMO_CARACTERES_DOCUMENTO = 20;

const FORMATO_FECHA_ENTRADA = 'yyyy/MM/dd';
const FORMATO_FECHA_SALIDA = 'dd/MM/yyyy';

@Component({
  selector: 'app-form-employee',
  templateUrl: './form-employee.component.html',
})
export class FormEmployeeComponent implements OnInit {

  @Output() eventoClick = new EventEmitter<Employee>();
  @Output() eventoCancel = new EventEmitter();

  reactiveForm: FormGroup;
  @ViewChild('reactiveFormView', { static: false }) reactiveFormView: NgForm;

  documentTypes: Observable<DocumentType[]>;
  countries: Observable<Country[]>;
  areas: Observable<Area[]>;

  constructor(
    private _fb: FormBuilder,
    private _dateAdapter: DateAdapter<any>,
    private _documentTypeService: DocumentTypeService,
    private _countryService: CountryService,
    private _areaService: AreaService,
    private _datePipe: DatePipe,
    private _toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.builderForm();
    this.loadCombox();
  }

  emitClickSave() {

    if(this.reactiveForm.invalid) {
      this._toastr.error('Diligencie correctamente todos los campos')
      return;
    }

    const formData = this.reactiveForm.getRawValue();

    formData.fechaIngreso =  this._datePipe.transform(new Date(formData.fechaIngreso), FORMATO_FECHA_SALIDA);

    this.eventoClick.emit(formData)
  }

  emmitClickCancel(){
    this.eventoCancel.emit();
  }

  clearForm() {
    this.reactiveForm.reset();
  }

  loadData(employee: any) {
    console.log(employee);
    const items = employee.fechaIngreso.split('/');
    employee.fechaIngreso = new Date(`${items[2]}/${items[1]}/${items[0]}`);
    console.log(employee);
    this.reactiveForm.patchValue(employee);
  }

  private builderForm() {
    this.reactiveForm = this._fb.group({
      id: ['',],
      primerApellido: ['', [Validators.required,
      Validators.maxLength(MAXIMO_CARACTERES_PRIMER_APELLIDO)]],
      segundoApellido: ['', [Validators.required,
      Validators.maxLength(MAXIMO_CARACTERES_SEGUNDO_APELLIDO)]],
      primerNombre: ['', [Validators.required,
        Validators.maxLength(MAXIMO_CARACTERES_PRIMER_NOMBRE)]],
      otrosNombres: ['', [Validators.required,
        Validators.maxLength(MAXIMO_CARACTERES_OTROS_NOMBRE)]],
      pais: ['', [Validators.required]],
      estado: ['', [Validators.required]],
      tipoIdentificacion: ['', [Validators.required]],
      numeroIdentificacion: ['', [Validators.required,
        Validators.maxLength(MAXIMO_CARACTERES_DOCUMENTO)]],
      correo: [''],
      fechaIngreso: ['', [Validators.required]],
      fechaRegistro: [''],
      fechaEdicion: [''],
      area: ['', [Validators.required]],

    });

    this.reactiveForm.get('correo').disable();
    this.reactiveForm.get('fechaRegistro').disable();
    this.reactiveForm.get('fechaEdicion').disable();
  }

  private loadCombox() {
    this.documentTypes  = this._documentTypeService.all();
    this.countries = this._countryService.all();
    this.areas = this._areaService.all();
  }




}
