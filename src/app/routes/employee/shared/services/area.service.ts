import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Area } from '../models/area';


@Injectable()
export class AreaService {

  all(): Observable<Area[]> {
    return of([
      { id: 'ad', nombre: 'Administración' },
      { id: 'fi', nombre: 'Financiera' },
      { id: 'co', nombre: 'Compras' },
      { id: 'in', nombre: 'Infraestructura' },
      { id: 'op', nombre: 'Operación' },
      { id: 'th', nombre: 'Talento Humano' },
      { id: 'se', nombre: 'Servicios Varios' },
    ])
  }

}
