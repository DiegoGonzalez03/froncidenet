import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Country } from '../models/country';


@Injectable()
export class CountryService {

  all(): Observable<Country[]> {
    return of([
      { id: 'co', nombre: 'Colombia' },
      { id: 'eeuu', nombre: 'Estados Unidos' }
    ])
  }

}
