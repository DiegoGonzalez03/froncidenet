import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { DocumentType } from '../models/document-type';

@Injectable()
export class DocumentTypeService {



  all(): Observable<DocumentType[]> {
    return of([
      { id: 'cc', nombre: 'Cédula de Ciudadanía' },
      { id: 'ce', nombre: 'Cédula de Extranjería' },
      { id: 'pa', nombre: 'Pasaporte' },
      { id: 'pe', nombre: 'Permiso Especial' }
    ])
  }

}
