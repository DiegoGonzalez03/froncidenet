import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiRequestService } from '@core/api-request.service';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Employee } from '../models/employee';
import { UsuarioPage } from '../models/usuario-page';

@Injectable()
export class UsuarioService {

  private path = `${environment.apiResource}/usuario`;

  constructor(private _apiRequestService: ApiRequestService) { }

  paginar(currentPage: number): Observable<UsuarioPage> {
    const config = {
      urlParams: new HttpParams().append('paginaActual', String(currentPage))
    };
    return this._apiRequestService.get(`${this.path}/`, config);
  }

  register(usuario: Employee): Observable<void> {
    return this._apiRequestService.post(`${this.path}/`, usuario)

  }

  update(idUsuario: string, usuario: Employee): Observable<void> {
    return this._apiRequestService.put(`${this.path}/${idUsuario}`, usuario);

  }

  delete(idUsuario: string): Observable<void> {
    return this._apiRequestService.delete(`${this.path}/${idUsuario}`);
  }

}
