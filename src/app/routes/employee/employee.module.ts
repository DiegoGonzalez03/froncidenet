import { DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from 'app/material.module';
import { EmployeeRoutingModule } from './employee-routing.module';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';
import { FormEmployeeComponent } from './shared/component/form-employee/form-employee.component';
import { AreaService } from './shared/services/area.service';
import { CountryService } from './shared/services/country.service';
import { DocumentTypeService } from './shared/services/document-type.service';
import { UsuarioService } from './shared/services/usuario.service';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';

const COMPONENTS = [ListEmployeeComponent, FormEmployeeComponent, RegisterEmployeeComponent, UpdateEmployeeComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    EmployeeRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_DYNAMIC
  ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [UsuarioService, DocumentTypeService, CountryService, AreaService, DatePipe],
})
export class EmployeeModule { }
